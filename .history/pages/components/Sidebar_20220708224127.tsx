import Tooltip from "@mui/material/Tooltip";
import Avatar from "@mui/material/Avatar";
import styled from "styled-components";

const StyledContainer = styled.div``;

const StyledSearch = styled.div``;

const StyledHeader = styled.div``;

const StyledSidebarButton = styled.button``;

const StyledUserAvatar = styled(Avatar)`
  cursor: pointer;
  :hover {
    opacity: 0.8;
  }
`;

const Sidebar = () => {
  return (
    <StyledContainer>
      <StyledHeader>
        <Tooltip title="USER EMAIL">
          <StyledUserAvatar />
        </Tooltip>
      </StyledHeader>

      <StyledSearch></StyledSearch>

      <StyledSidebarButton></StyledSidebarButton>

      {/* List of conversations */}
    </StyledContainer>
  );
};

export default Sidebar;
