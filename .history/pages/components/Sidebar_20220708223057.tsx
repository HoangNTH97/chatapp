import styled from "styled-components";
const StyledContainer = styled;

const Sidebar = () => {
  return (
    <StyledContainer>
      <StyledHeader></StyledHeader>

      <StyledSearch></StyledSearch>

      <StyledSidebarButton></StyledSidebarButton>
    </StyledContainer>
  );
};

export default Sidebar;
