import styled from "styled-components";

const StyledContainer = styled.div``;
const StyledSearch = styled.div``;
const StyledHeader = styled.div``;
const StyledSidebarButton = styled.button``;

const Sidebar = () => {
  return (
    <StyledContainer>
      <StyledHeader></StyledHeader>

      <StyledSearch></StyledSearch>

      <StyledSidebarButton></StyledSidebarButton>

      {/* List of conversations */}
    </StyledContainer>
  );
};

export default Sidebar;
