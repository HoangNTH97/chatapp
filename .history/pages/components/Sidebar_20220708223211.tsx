import styled from "styled-components";

const StyledContainer = styled.div``;
const StyledSearch = styled.div``;
const StyledHeader = styled.div``;
const StyledSidebarButton = styled.div``;

const Sidebar = () => {
  return (
    <StyledContainer>
      <StyledHeader></StyledHeader>

      <StyledSearch></StyledSearch>

      <StyledSidebarButton></StyledSidebarButton>
    </StyledContainer>
  );
};

export default Sidebar;
