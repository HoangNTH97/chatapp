// Thiết lập cấu hình cho firebase

// Import the functions you need from the SDKs you need
import { getApp, getApps, initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCjQDekZL0Kq9L6jGpvaMUF4ooedUQ4nUU",
  authDomain: "whatsapp-clone-8ff2d.firebaseapp.com",
  projectId: "whatsapp-clone-8ff2d",
  storageBucket: "whatsapp-clone-8ff2d.appspot.com",
  messagingSenderId: "689480781445",
  appId: "1:689480781445:web:ee79e5bb523bb7cd18e3ec",
};

// Initialize Firebase
const app = getApps().length ? getApp() : initializeApp(firebaseConfig);

const db = getFirestore(app);

const auth = getAuth(app);

const provider = new GoogleAuthProvider();

export { db, auth, provider };
